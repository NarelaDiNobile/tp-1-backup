using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TiempoJuego : MonoBehaviour
{
    public TMP_Text txt_cronometro;
    int temporizador =0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Cronometro");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator Cronometro()
    {
        int minutos = 0;
        int segundos = 0;
        temporizador = 240;
        while (temporizador > 0)
        {
            if (txt_cronometro.text != "Ganaste")
            {
                minutos = temporizador / 60;
                segundos = temporizador % 60;
                txt_cronometro.text = "" +  minutos + ":" + segundos;
            }
            yield return new WaitForSeconds(1.0f);
            temporizador--;
        }
       
        if (txt_cronometro.text != "Ganaste")
        {
            
            StartCoroutine("Esperar");

        }

    }
}
