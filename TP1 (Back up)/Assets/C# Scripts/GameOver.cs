using System.Collections;
using System.Collections.Generic;
using System.Reflection;
//using System.Runtime.Hosting;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    private JugadorPierdeVidas scriptJPV;
    private ContadorVidas scriptCV;
    public Image Img_GameOver;

    // Start is called before the first frame update
    void Start()
    {
        scriptJPV = FindObjectOfType<JugadorPierdeVidas>();
        scriptCV = FindObjectOfType<ContadorVidas>();

        Img_GameOver.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(scriptCV.cantidad_vidas ==0)
        {
            Img_GameOver.gameObject.SetActive(true);
            StartCoroutine("Esperar");
            //Application.Quit();
        }
    }

    private IEnumerator Esperar()
    {
        int tiempo_espera = 3;
        while(tiempo_espera > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
        Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }




}
