using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class JugadorMovimiento : MonoBehaviour
{
    Rigidbody miRB;
    public int rapidez;

    void Start()
    {
        miRB = GetComponent<Rigidbody>();
        rapidez = 3;
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidez;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidez;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        miRB.transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }
}
