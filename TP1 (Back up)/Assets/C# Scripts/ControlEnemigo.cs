using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigo : MonoBehaviour
{
    private WakeUpTrigger scriptWUT;

    public GameObject jugadorrr;
    int rapidez = 1;
    // Start is called before the first frame update
    void Start()
    {
        scriptWUT = FindObjectOfType<WakeUpTrigger>();
    }

    // Update is called once per frame
    void Update()
    {
        if (scriptWUT.wakeup == true)
        {
            transform.LookAt(jugadorrr.transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
        
    }
}
