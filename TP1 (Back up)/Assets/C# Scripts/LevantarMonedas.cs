using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevantarMonedas : MonoBehaviour
{
    
    public TMP_Text txt_CantMone;
    private ContadorMonedas scriptContarMone;
    void Start()
    {
        scriptContarMone = FindObjectOfType<ContadorMonedas>();
    }


    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        this.gameObject.SetActive(false);
        scriptContarMone.contador_monedas += 1;
        txt_CantMone.text = " $ " + scriptContarMone.contador_monedas + "/29";
    }
}
