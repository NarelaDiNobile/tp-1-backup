using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public string sceneName;

    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void playGame()
    {
        SceneManager.LoadScene(sceneName);
    }
    public void quitGame()
    {
        Debug.Log("Has salido");
        Application.Quit();
    }
}
