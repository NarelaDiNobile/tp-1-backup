using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WakeUpTrigger : MonoBehaviour
{
    public bool wakeup;
    // Start is called before the first frame update
    void Start()
    {
        wakeup  = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            wakeup = true;
        }
    }
}
