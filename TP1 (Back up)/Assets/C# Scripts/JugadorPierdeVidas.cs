using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class JugadorPierdeVidas : MonoBehaviour
{
    private ContadorVidas scriptCV;
    public TMP_Text txt_cantidad_vidas;
    // Start is called before the first frame update
    void Start()
    {
        scriptCV = FindObjectOfType<ContadorVidas>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        //scriptCV.cantidad_vidas -= 1;
        if (collision.gameObject.tag == "Enemigo")
        {
            collision.gameObject.SetActive(false);
            scriptCV.cantidad_vidas -= 1;
            txt_cantidad_vidas.text = "Vidas: " + scriptCV.cantidad_vidas + "/3";
        }
    }
}
