using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class AudioAgua : MonoBehaviour
{
    public AudioSource QuienEmite;
    public AudioClip Agua;
    public float volumen = 1;
    
    void Start()
    {
        
    }


    void Update()
    {
        
    }

    private void OnTriggerEnter (Collider other)
    {
        QuienEmite.PlayOneShot(Agua, volumen);
    }

    private void OnTriggerExit(Collider other)
    {
        QuienEmite.PlayOneShot(Agua, volumen);
    }
}
