using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class JugadorSalto : MonoBehaviour
{
    public float fuerzaSalto = 5f;
    public float distanciaHastaElSuelo = 1f;
    private int CantSaltosDisponibles;
    public LayerMask capaDeSalto;
    private bool estaEnElSuelo;
    private Rigidbody miRB;
    
    // Start is called before the first frame update
    void Start()
    {
        CantSaltosDisponibles = 2;
        miRB = GetComponent<Rigidbody>();
    }
    

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && (CantSaltosDisponibles > 0))
        {
            if (estaEnElSuelo)
            {
                miRB.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
                CantSaltosDisponibles -= 1;
            }
            if((!estaEnElSuelo) && Input.GetKeyDown(KeyCode.Space)&& (CantSaltosDisponibles > 0))
            {
                miRB.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
                CantSaltosDisponibles -= 1;
            }
        }

    }


    void FixedUpdate()
    {
        //detecta si el jugador esta en el suelo 
        estaEnElSuelo = Physics.Raycast(transform.position, Vector3.down, distanciaHastaElSuelo, capaDeSalto);
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Objetos de Salto"))
        {
            CantSaltosDisponibles = 2;
        }
    }



}
