using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class JugadorWallJump : MonoBehaviour
{

    public GameObject Plataforma14;
    public GameObject Plataforma15;
    public GameObject Plataforma16;
    public GameObject Plataforma17;
    public GameObject Plataforma18;
    public bool isOn = false;
    
    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKey("f"))
        {
            StartCoroutine(WallRun());
        }

        if (Input.GetKeyUp("f"))
        {
            StartCoroutine(Stop());
        }
    }

    IEnumerator WallRun()
    {
        isOn = true;
        Plataforma14.SetActive(true);
        Plataforma15.SetActive(true);
        Plataforma16.SetActive(true);
        Plataforma17.SetActive(true);
        Plataforma18.SetActive(true);
        yield return new WaitForSeconds(0f);
    }

    IEnumerator Stop()
    {
        isOn = false;
        Plataforma14.SetActive(false);
        Plataforma15.SetActive(false);
        Plataforma16.SetActive(false);
        Plataforma17.SetActive(false);
        Plataforma18.SetActive(false);
        yield return new WaitForSeconds(0f);
    }
}