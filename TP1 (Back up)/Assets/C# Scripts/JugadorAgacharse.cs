using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorAgacharse : MonoBehaviour
{
    private Rigidbody miRB;
    bool esta_agachado;

    // Start is called before the first frame update
    void Start()
    {
        miRB = GetComponent<Rigidbody>();
        esta_agachado = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.C) && esta_agachado == false)
        {
            esta_agachado = true;
            miRB.transform.localScale = new Vector3(miRB.transform.localScale.x, miRB.transform.localScale.y / 4, miRB.transform.localScale.z);
        }
        else if(Input.GetKeyDown(KeyCode.C) && esta_agachado == true)
        {
            esta_agachado = false;
            miRB.transform.localScale = new Vector3(miRB.transform.localScale.x, miRB.transform.localScale.y * 4, miRB.transform.localScale.z);
        }
    }
}
