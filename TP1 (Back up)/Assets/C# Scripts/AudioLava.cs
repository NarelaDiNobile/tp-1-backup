using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLava : MonoBehaviour
{
    public AudioSource QuienEmite;
    public AudioClip Lava;
    public float volumen = 1;

    void Start()
    {

    }


    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        QuienEmite.PlayOneShot(Lava, volumen);
    }

    private void OnTriggerExit(Collider other)
    {
        QuienEmite.PlayOneShot(Lava, volumen);
    }
}
