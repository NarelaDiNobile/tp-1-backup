using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class DesaparecerDespuesDeTiempo : MonoBehaviour
{

    public float tiempoDesaparecer = 1f;
    public float tiempoAparecer = 1f;
    private bool tocado = false;
    private float tiempoTocado = 0f;
    private float tiempoEspera = 0f;
    private Vector3 posicionOriginal;

    // Start is called before the first frame update
    void Start()
    {
        posicionOriginal = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (tocado)
        {
            tiempoTocado += Time.deltaTime;
            if(tiempoTocado >= tiempoDesaparecer)
            {
                gameObject.SetActive(false);
                tiempoEspera = tiempoAparecer;
            }
        }

        if (!gameObject.activeSelf)
        {
            tiempoEspera -= Time.deltaTime;
            if(tiempoEspera <= 0f)
            {
                transform.position = posicionOriginal;
                gameObject.SetActive(true);
                tocado = false;
                tiempoTocado = 0f;
            }
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            tocado = true;
        }
    }
}
