using System.Collections;
using System.Collections.Generic;
using System.Reflection;
//using System.Runtime.Hosting;
using UnityEngine;
using UnityEngine.UI;

public class GanarFinJuego : MonoBehaviour
{
    bool fin_juego = false;
    public Image Img_Winner;
    // Start is called before the first frame update
    void Start()
    {
        Img_Winner.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        fin_juego = true;
        Img_Winner.gameObject.SetActive(true);
        StartCoroutine("Esperar");
    }

    private IEnumerator Esperar()
    {
        int tiempo_espera = 3;
        while (tiempo_espera >0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
        Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }
}
